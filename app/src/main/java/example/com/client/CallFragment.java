package example.com.client;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Nathan on 07/04/2016.
 */
public class CallFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_call, container, false);

        // make the log scrollable & clear the lorem ipsum text
        TextView logView = (TextView) view.findViewById(R.id.log);
        logView.setMovementMethod(new ScrollingMovementMethod());
        logView.setText("");

        // wire up the dial / hangup
        Button dialButton = (Button) view.findViewById(R.id.dial);
        dialButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // find the address the user dialled
                EditText addressField = (EditText) view.findViewById(R.id.address);
                String address = addressField.getText().toString();

                addMessageToLog("User dialled: " + address);
            }
        });

        Button hangupButton = (Button) view.findViewById(R.id.hangup);
        hangupButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // hangup a call (if we're on one)
                addMessageToLog("User clicked to hangup");
            }
        });


        // wire up the local audio / video & camera change check boxes
        CheckBox   audioCheckBox = (CheckBox)view.findViewById(R.id.audio);
        CheckBox   videoCheckBox = (CheckBox)view.findViewById(R.id.video);
        CheckBox rearCamCheckBox = (CheckBox)view.findViewById(R.id.rear_cam);

        audioCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable audio: " + checked);
            }
        });

        videoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable video: " + checked);
            }
        });

        rearCamCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Use rear cam: " + checked);
            }
        });

        // return the prepared statement
        return view;
    }

    // adds a message to the log
    private void addMessageToLog(final String message) {
        final TextView textView = (TextView) this.getView().findViewById(R.id.log);
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                textView.setText(message + "\n" + textView.getText());
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getView().getWindowToken(), 0);
    }

}
