package example.com.client;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * Created by Nathan on 07/04/2016.
 */
public class ConfigFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // inflate the correct view
        final View view = inflater.inflate(R.layout.fragment_config, container, false);

        // turn off the progress bar visibility
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);

        // setup the button click listener
        final Button startStopButton = (Button) view.findViewById(R.id.start_stop);
        startStopButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // hide the keyboard
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                // start the task to poll the server for the session ID, and
                EditText srvrText = (EditText) view.findViewById(R.id.server);
                EditText userText = (EditText) view.findViewById(R.id.username);
                EditText passText = (EditText) view.findViewById(R.id.password);
                new LoginThread(
                        srvrText.getText().toString(),
                        userText.getText().toString(),
                        passText.getText().toString()
                ).start();

                // update the UI to indicate we're attempting a potentially long running task
                // make the buttons and fields disabled
                startStopButton.setEnabled(false);
                srvrText.setEnabled(false);
                userText.setEnabled(false);
                passText.setEnabled(false);

                // make the progress dial visible
                ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progress);
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        // return the view we've constructed
        return view;
    }


    // private class for handling the login operation
    private class LoginThread extends Thread {

        private String serverIp;
        private String user;
        private String password;

        public LoginThread (String serverIp, String user, String password) {
            this.serverIp = serverIp;
            this.user = user;
            this.password = password;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(5000);
                this.updateUI(true);
            } catch (InterruptedException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }

        private void updateUI (final boolean registered) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // set the button label
                    Button startStopButton = (Button) getActivity().findViewById(R.id.start_stop);
                    String label = registered
                            ? "Stop"
                            : "Start";

                    startStopButton.setText(label);

                    // find the UI views
                    EditText serverEditText = (EditText) getActivity().findViewById(R.id.server);
                    EditText usernameEditText = (EditText) getActivity().findViewById(R.id.username);
                    EditText passwordEditText = (EditText) getActivity().findViewById(R.id.password);
                    ProgressBar progressBar = (ProgressBar) getActivity().findViewById(R.id.progress);

                    // enable / disable as appropriate
                    startStopButton.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    serverEditText.setEnabled(!registered);
                    usernameEditText.setEnabled(!registered);
                    passwordEditText.setEnabled(!registered);

                    // pop a toast to give some feedback
                    String message = (registered) ? "Registered with server" : "Failed to register";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
